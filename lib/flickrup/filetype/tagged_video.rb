require 'yaml'
require 'flickrup/filetype/picasa_ini'
require "flickrup/logging"

class TaggedVideo < TaggedFile
  include Logging

  attr_reader :filename

  TYPE = 'video'

  def initialize(filename)
    super
    @filename = filename
    @properties = PicasaIni.open(filename)
  end

  # Returns the value of a tag.
  def [] tag
    vals = @properties[tag]
    if tag == "keywords"
      unless vals.nil?
        vals = vals.split(",")
      end
    end
    return vals
  end

  # Set the value of a tag.
  def []=(tag, val)
    @properties[tag] = val
  end

  def type
    TYPE
  end

  register_reader(%w(mp4 MP4 mov MOV))

end
