require "flickrup/filetype/tagged_file"
require "flickrup/uploader"
require "flickrup/logging"
require "flickrup/ext/tag_sets"
require "flickrup/ext/replacement_tags"
require "flickrup/ext/machine_tags"
require "flickrup/ext/visibility"
require "flickrup/ext/sidecar"
require "flickrup/ext/blocked_tags"
require "flickrup/archiver"
require "flickrup/ext/associated_files"
require 'flickrup/converter'

class Processor
  include Logging

  def initialize(config)
    @MAX_DATE = Time.at(2130578470)
    @config = config
    @converter = Converter.new(config[:convert_files])
  end

  def paused
    Dir["#{@config[:watch_dir]}/paused"].length > 0
  end

  def run(all_files = Dir["#{@config[:watch_dir]}/*.*"])

    logger.info("Processing #{all_files.length} files")

    if paused
      logger.info("Paused detected, so skipping processing...")
      return 0
    end

    converted_count = all_files.select{ |x|
      if File.exist? x
        @converter.maybe_convert(x)
      else
        false
      end
    }.length

    if converted_count > 0
      logger.info("Converted #{converted_count} files, reprocessing")
      return true
    end

    # some files may have been removed by now: https://bitbucket.org/jgilbert/flickrup/issue/1
    files = all_files.map { |x|
      if File.exist? x
        begin
          TaggedFile.create(x)
        rescue => e
          logger.error("Failed to load file #{x}: #{e}")
          logger.info("Aborting run")
          return false
        end
      else
        logger.debug("File #{x} already removed, skipping.")
      end
    }.compact

    logger.debug("Scanned #{files.length} files")

    by_date = files.sort_by { |x|
      x.date_created || @MAX_DATE
    }

    tagged = by_date.take_while { |x|
      !(x.tags.nil? || x.tags.empty?)
    }

    if tagged.empty?
      logger.info('No files selected for upload')
      unless by_date.empty?
        logger.debug("Stopped on #{by_date[0].filename}")
      end
    else
      logger.info("Selected #{tagged.length} files for upload")
    end

    uploader = get_uploader

    if @config[:dryrun]
      logger.info("Would now upload & move #{tagged.length} files, pausing for 5 secs instead")
      sleep 5
    else
      tagged.each do |x|
        begin
          logger.info("Beginning upload for #{x.filename}...")
          unless x.doUpload(get_preupload_handlers, uploader)
            break #abort
          end
          logger.info("Upload complete for #{x.filename}")
          logger.info("Beginning archive for #{x.filename}...")
          x.archive(get_archiver, @config[:archive_dir])
          logger.info("Processing complete for #{x.filename}")
        rescue => e
          logger.error("Failed to process #{x.filename} due to exception: #{e}")
          logger.info(e.backtrace.join("\n"))
        end
      end
    end

    logger.info("Processed #{tagged.length} valid files")

    false
  end

  private
  def get_uploader

    unless @uploader

      context = get_chain_context()

      uploader = Uploader.new(context[:flickr_client])

      @uploader = upload_handlers.reduce(uploader) do |result, clazz|
        clazz.new(@config, result, context)
      end
    end

    return @uploader
  end

  def get_archiver
    unless @archiver
      @archiver = archive_handlers.reduce(Archiver.new) do |result, clazz|
        clazz.new(@config, result, get_chain_context)
      end
    end
    return @archiver
  end

  def get_chain_context
    unless @chain_context
      flickr_client = FlickrClient.new(@config[:api_key], @config[:shared_secret])
      @chain_context = {:flickr_client => flickr_client}
    end

    return @chain_context
  end

  def get_preupload_handlers
    [MachineTags.new(@config, {}), ReplacementTags.new(@config), BlockedTags.new(@config)]
  end

  def upload_handlers
    [TagSets, Visibility]
  end

  def archive_handlers
    [TagSets, Sidecar, AssociatedFiles]
  end

end

